


### 1.[商城后台管理系统](http://www.macrozheng.com/#/foreword/mall_foreword_01)

![在这里插入图片描述](https://images.gitee.com/uploads/images/2020/0522/142724_89dfbb08_5128445.png)



### 2.[林鑫的博客](https://lin-xin.gitee.io/) 

#### [GitHub](https://github.com/lin-xin)  

![在这里插入图片描述](https://images.gitee.com/uploads/images/2020/0522/142725_c601ce4d_5128445.png)

### 3.[若依的博客](http://zmrit.com/)

#### [GitHub](https://github.com/zhangmrit)

###### [文档](http://doc.rycloud.zmrit.com/#/)

![在这里插入图片描述](https://images.gitee.com/uploads/images/2020/0522/142725_d8707637_5128445.png)

### 4.[花裤衩](https://segmentfault.com/u/panjiachen)

##### [GitHub](https://github.com/PanJiaChen)

###### [文档](https://panjiachen.github.io/vue-element-admin-site/zh/guide/#%E5%8A%9F%E8%83%BD)

![在这里插入图片描述](https://images.gitee.com/uploads/images/2020/0522/142725_1c1b3e56_5128445.png)

### 5.taylor

##### [GitHub](https://github.com/taylorchen709)

![在这里插入图片描述](https://images.gitee.com/uploads/images/2020/0522/142725_fee2275c_5128445.png)

[更多](https://blog.csdn.net/m0_38106923/article/details/101050788)

### 6.[OBKoro1](http://obkoro1.com/)  

###### [文档](http://obkoro1.com/web_accumulate/codeBlack/vuex%E4%B8%89%E6%AD%A5.html)



# 7.后台管理系统


#### [elune/eladmin](https://gitee.com/elunez/eladmin)

[demo](https://auauz.net/login?redirect=/dashboard)

![在这里插入图片描述](https://images.gitee.com/uploads/images/2020/0522/142725_8af75cbb_5128445.png)


### 7.[iview-admin](https://github.com/lzugis/iview-admin) iview 后台系统

[技术博客](https://lison16.github.io/iview-admin-doc/#/%E7%AE%80%E4%BB%8B?id=%E7%9B%AE%E5%BD%95%E7%BB%93%E6%9E%84)

[官网](https://iview.github.io/)

[demo](https://admin.iviewui.com/login)查看

[万威国际](http://lucky_ck.gitee.io/project-files/IDT%E4%B8%87%E5%A8%81%E5%9B%BD%E9%99%85/)


#### 简易的命令行入门教程:

Git 全局设置:

```
git config --global user.name "一切都是浮云"
git config --global user.email "1049906948@qq.com"
```

创建 git 仓库:

```
mkdir shopping_mall_project   新建目录
cd shopping_mall_project  进入目录
git init   创建仓库
touch README.md   创建文件
git add README.md    添加文件
git commit -m "first commit"  提交 文件     first commit首页提交

git remote add origin https://gitee.com/lucky_ck/shopping_mall_project.git  添加码云地址 
git push -u origin master   推送到主分支
```

已有仓库?

```
cd existing_git_repo
git remote add origin https://gitee.com/lucky_ck/shopping_mall_project.git    添加码云地址 
git push -u origin master    推送到主分支
```

跟新仓库

```
git status  查看状态
git add .可以提交未跟踪和修改文件，但是不处理删除文件。
git add all可以提交未跟踪、修改和删除文件。
git commit -m  "add files"    把它提交到版本库
```

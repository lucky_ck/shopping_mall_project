'use strict'
// Template version: 1.3.1
// see http://vuejs-templates.github.io/webpack for documentation.

const path = require('path')

module.exports = {
  dev: {

    // Paths
    assetsSubDirectory: 'static',
    assetsPublicPath: '/',
    proxyTable: {},

    // Various Dev Server settings 各种开发服务器设置
    host: 'localhost', // can be overwritten by process.env.HOST  可以被process.env.HOST覆盖
    port: 8080, // 如果端口正在使用，则将确定空闲端口
    autoOpenBrowser: false,
    errorOverlay: true,
    notifyOnErrors: true,
    poll: false, // https://webpack.js.org/configuration/dev-server/#devserver-watchoptions-


    /**
     * Source Maps
     */

    // https://webpack.js.org/configuration/devtool/#development
    devtool: 'cheap-module-eval-source-map',

    // 如果在devtools中调试vue文件时遇到问题
    // 把这个设为false-它*可能*有帮助
    // https://vue-loader.vuejs.org/en/options.html#cachebusting
    cacheBusting: true,

    cssSourceMap: true
  },

  build: {
    // Template for index.html
    index: path.resolve(__dirname, '../dist/index.html'),

    // Paths
    assetsRoot: path.resolve(__dirname, '../dist'),
    assetsSubDirectory: 'static',
    assetsPublicPath: './',

    /**
     * Source Maps
     */

    productionSourceMap: true,
    // https://webpack.js.org/configuration/devtool/#production
    devtool: '#source-map',

    // 默认情况下，关闭Gzip，因为许多流行的静态主机，如
    // 浪涌或Netlify已经为您gzip所有静态资产。
    // 在设置为“true”之前，请确保：
    // npm install --save-dev compression-webpack-plugin
    productionGzip: false,
    productionGzipExtensions: ['js', 'css'],

    // 运行带有额外参数的build命令
    // 生成完成后查看bundle analyzer报告：
    // `npm run build --report`
    // 设置为“true”或“false”以始终将其打开或关闭
    bundleAnalyzerReport: process.env.npm_config_report
  }
}
